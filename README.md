## System requirements:

- Node.js 12.22.0 or later
- Docker
- MacOS, Windows (including WSL), and Linux are supported

## Getting Started
First you need to clone the following repo:

```bash
git clone https://bitbucket.org/adichallenge/product-reviews-docker-composer/src/master/
```

Next run docker: 

```bash
docker-compose up
```

Then, run the development server:

```bash
npm install
npm run dev
# or
yarn install
yarn dev
```

Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.

If port 8080 is not available change **:PORT** in `.env.local` and start the app with 

```bash
npm run dev -p :PORT
# or
yarn dev -p :PORT
```
