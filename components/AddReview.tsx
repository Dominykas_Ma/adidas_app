import React, { useState } from 'react';
import { useDisclosure } from '@chakra-ui/hooks';
import {
  Alert,
  AlertIcon,
  Button,
  FormLabel,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Textarea,
} from '@chakra-ui/react';
import { useSetProductReview } from '../queries/reviewQueries';

type AddReviewProps = {
  productId: string;
};

const AddReview = (props: AddReviewProps) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [rating, setRating] = useState(1);
  const [review, setReview] = useState('');
  const { mutate, isError } = useSetProductReview({
    options: {
      onSuccess: () => onClose(),
    },
  });

  const handleSubmit = () => {
    if (review) {
      mutate({ productId: props.productId, rating, text: review });
    }
  };

  return (
    <>
      <Button onClick={onOpen} data-testid="add-review-button">
        Add a review
      </Button>
      <Modal onClose={onClose} isOpen={isOpen} isCentered size={'xl'}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add review</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {isError && (
              <Alert status="error">
                <AlertIcon borderRadius={5} />
                There was an error processing your request, please try again
              </Alert>
            )}
            <FormLabel htmlFor="rating">Rating</FormLabel>
            <NumberInput
              defaultValue={1}
              min={1}
              max={10}
              mb={5}
              onChange={(value) => setRating(parseInt(value))}
            >
              <NumberInputField data-testid="add-review-number-input" />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>

            <FormLabel htmlFor="review">Review</FormLabel>
            <Textarea
              isRequired
              minH={150}
              placeholder="Please leave your product review here"
              onChange={(e) => setReview(e.target.value)}
              data-testid="add-review-textarea"
            />
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleSubmit}>Submit</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AddReview;
