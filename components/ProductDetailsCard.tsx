import React from 'react';
import { Box, Image, Flex, SimpleGrid, Stack, Text, Heading, chakra } from '@chakra-ui/react';

type ProductDetailsCardProps = {
  id: string;
  name: string;
  currency: string;
  price: number;
  description: string;
  imgUrl: string;
};

const ProductDetailsCard = (props: ProductDetailsCardProps) => {
  return (
    <Box pt={120} maxW={'5xl'}>
      <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
        <Flex>
          <Image rounded={'md'} alt={'feature image'} src={props.imgUrl} objectFit={'cover'} />
        </Flex>
        <Stack spacing={4}>
          <Text
            textTransform={'uppercase'}
            color={'gray.50'}
            fontWeight={600}
            fontSize={'sm'}
            bg={'blue.900'}
            p={2}
            alignSelf={'flex-start'}
            rounded={'md'}
          >
            {`SKU: ${props.id}`}
          </Text>
          <chakra.h1
            color={'white'}
            fontWeight="bold"
            fontSize="3xl"
            textTransform="uppercase"
            textAlign={{ base: 'center', md: 'left' }}
          >
            {props.name}
          </chakra.h1>
          <chakra.h1 color="white" fontWeight="bold" fontSize="2xl">
            {`${props.price} ${props.currency}`}
          </chakra.h1>
          <Text color={'gray.500'} fontSize={'lg'}>
            {props.description}
          </Text>
        </Stack>
      </SimpleGrid>
    </Box>
  );
};

export default ProductDetailsCard;
