import { Image } from '@chakra-ui/react';
import React from 'react';

export const Logo = () => <Image src="/adidas_logo.svg" alt="Adidas Logo" minW={65} minH={43} />;
