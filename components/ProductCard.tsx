import React from 'react';
import { chakra, Box, Image, Flex, Link } from '@chakra-ui/react';

type ProductCardProps = {
  id: string;
  name: string;
  currency: string;
  price: number;
  description: string;
  imgUrl: string;
};

const ProductCard = (props: ProductCardProps) => {
  return (
    <Box width="xs" bg={'gray.800'} shadow="lg" rounded="lg" margin={'auto'}>
      <Box px={4} py={2}>
        <chakra.h1 color={'white'} fontWeight="bold" fontSize="3xl" textTransform="uppercase">
          {props.name}
        </chakra.h1>
        <chakra.p mt={1} fontSize="sm" color={'gray.400'}>
          {props.description}
        </chakra.p>
      </Box>

      <Image h={80} w="full" fit="cover" mt={2} src={props.imgUrl} alt="Product image" />

      <Flex alignItems="center" justifyContent="space-between" px={4} py={2} bg="gray.900" roundedBottom="lg">
        <chakra.h1 color="white" fontWeight="bold" fontSize="lg">
          {`${props.price} ${props.currency}`}
        </chakra.h1>
        <Link href={`/product/${props.id}`}>
          <chakra.button
            px={2}
            py={1}
            bg="white"
            fontSize="xs"
            color="gray.900"
            fontWeight="bold"
            rounded="lg"
            textTransform="uppercase"
            _hover={{
              bg: 'gray.200',
            }}
            _focus={{
              bg: 'gray.400',
            }}
          >
            View item
          </chakra.button>
        </Link>
      </Flex>
    </Box>
  );
};

export default ProductCard;
