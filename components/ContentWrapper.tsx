import React, { ReactNode } from 'react';
import { Box, Flex } from '@chakra-ui/react';

const ContentWrapper: React.FC<{
  children?: ReactNode;
  color?: string;
}> = (props) => (
  <Box background={props.color} px={{ base: 6, sm: 8 }} py={75} className={'wrapper'}>
    <Flex direction={'column'} basis={'auto'} w="full" maxW={1400} m="auto">
      {props.children}
    </Flex>
  </Box>
);

export default ContentWrapper;
