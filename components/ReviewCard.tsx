import { chakra, Flex, Image } from '@chakra-ui/react';
import React from 'react';

type ReviewCardProps = {
  rating: number;
  text: string;
};

const ReviewCard = (props: ReviewCardProps) => {
  return (
    <Flex
      boxShadow={'white'}
      maxW={'640px'}
      minW={{ md: '500px' }}
      direction={{ base: 'column-reverse', md: 'row' }}
      width={'full'}
      rounded={'xl'}
      p={10}
      justifyContent={'space-between'}
      position={'relative'}
      bg={'gray.800'}
    >
      <Flex direction={'column'} textAlign={'left'} justifyContent={'space-between'}>
        <Flex pb={5}>
          {[...Array(props.rating)].map((_, index) => (
            <Image src="/star_icon.svg" alt="star" w={5} key={index} />
          ))}
        </Flex>
        <chakra.p fontFamily={'Inter'} fontWeight={'medium'} fontSize={'15px'} pb={4}>
          {props.text}
        </chakra.p>
      </Flex>
    </Flex>
  );
};

export default ReviewCard;
