import React from 'react';
import { useRouter } from 'next/router';
import {
  chakra,
  Flex,
  VisuallyHidden,
  HStack,
  InputGroup,
  InputLeftElement,
  Input,
  Box,
  Button,
} from '@chakra-ui/react';
import { AiOutlineSearch } from 'react-icons/ai';
import { Logo } from './Logo';
import { ArrowBackIcon } from '@chakra-ui/icons';

const Navbar: React.FC<{
  type: 'SEARCH' | 'BACK';
  onChange?: (value: string) => void;
}> = (props) => {
  const router = useRouter();
  return (
    <Box
      as="header"
      width="full"
      position="fixed"
      px={{ base: 6, sm: 8 }}
      py={4}
      background={'gray.800'}
      style={{ zIndex: 2 }}
      data-testid="Navbar"
    >
      <Flex alignItems="center" justifyContent="space-between" w="full" maxW={1400} m="auto">
        <HStack display="flex" alignItems="center" marginRight={10}>
          <chakra.a href="/" title="Choc Home Page" display="flex" alignItems="center">
            <Logo />
            <VisuallyHidden>Adidas</VisuallyHidden>
          </chakra.a>
        </HStack>

        <HStack alignItems="center">
          {props.type === 'SEARCH' ? (
            <InputGroup>
              <InputLeftElement pointerEvents="none" children={<AiOutlineSearch />} />
              <Input
                type="tel"
                placeholder="Search..."
                onChange={(e) => (props.onChange ? props.onChange(e.target.value) : undefined)}
                data-testid="Navbar-search"
              />
            </InputGroup>
          ) : (
            <Button
              leftIcon={<ArrowBackIcon />}
              data-testid="Navbar-back-button"
              onClick={() => router.back()}
            >
              Back
            </Button>
          )}
        </HStack>
      </Flex>
    </Box>
  );
};

export default Navbar;
