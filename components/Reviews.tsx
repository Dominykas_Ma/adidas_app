import { Box, chakra, Flex, SimpleGrid } from '@chakra-ui/react';
import { ReviewModel } from '../queries/types';
import ReviewCard from './ReviewCard';

type ReviewsProps = {
  reviews: ReviewModel[];
};

const Reviews = (props: ReviewsProps) => {
  return (
    <Flex justifyContent={'center'} direction={'column'} width={'full'}>
      <Box width={{ base: 'full', sm: 'lg', lg: 'xl' }} mt={16}>
        <chakra.h1 py={5} fontSize={48} fontWeight={'bold'} color={'gray.50'}>
          Reviews
        </chakra.h1>
      </Box>
      {props.reviews.length > 0 ? (
        <SimpleGrid columns={{ base: 1, xl: 2 }} spacing={'20'} mt={16} mx={'auto'}>
          {props.reviews.map((review, index) => (
            <ReviewCard key={index} rating={review.rating} text={review.text} />
          ))}
        </SimpleGrid>
      ) : (
        <chakra.h2 py={5} fontSize={30} color={'gray.50'}>
          This product has no reviews...
        </chakra.h2>
      )}
    </Flex>
  );
};

export default Reviews;
