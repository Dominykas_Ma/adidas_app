import { render, screen } from '@testing-library/react';
import AddReview from '../AddReview';
import { fireEvent } from '@testing-library/dom';
import { QueryClient, QueryClientProvider } from 'react-query';

describe('addReview component', () => {
  it('should open addReview modal on button click', async () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <AddReview />
      </QueryClientProvider>
    );
    const addReviewButton = screen.getByTestId('add-review-button');
    fireEvent.click(addReviewButton);
    expect(screen.getByText('Add review')).toBeInTheDocument();
  });

  it('should open addReview modal and prefill input fields', async () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <AddReview />
      </QueryClientProvider>
    );
    fireEvent.click(screen.getByTestId('add-review-button'));
    const numberInput = screen.getByTestId('add-review-number-input');
    const textarea = screen.getByTestId('add-review-textarea');
    fireEvent.change(numberInput, { target: { value: '9' } });
    fireEvent.change(textarea, { target: { value: 'Test Review' } });
    expect(numberInput.value).toBe('9');
    expect(textarea.value).toBe('Test Review');
  });
});
