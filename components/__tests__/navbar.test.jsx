import { render, screen } from '@testing-library/react';
import Navbar from '../Navbar';
import { fireEvent } from '@testing-library/dom';

describe('Navbar component', () => {
  it('should render Navbar', () => {
    render(<Navbar type={'SEARCH'} />);
    const navbar = screen.getByTestId('Navbar');
    expect(navbar).toBeInTheDocument();
  });

  it('should invoke onChange function', () => {
    render(<Navbar type={'SEARCH'} onChange={jest.fn} />);
    const navbarSearch = screen.getByTestId('Navbar-search');
    fireEvent.change(navbarSearch, { target: { value: 'test' } });
    expect(navbarSearch.value).toBe('test');
  });
});
