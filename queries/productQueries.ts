import { ProductModel } from './types';
import unfetch from 'isomorphic-unfetch';
import { useQuery, UseQueryOptions } from 'react-query';

export const getProducts = async () => {
  const response = await unfetch(`${process.env.BASE_URL}/api/products`);
  const result: ProductModel[] = await response.json();
  return result;
};

export const getProduct = async (id: string) => {
  const response = await unfetch(`${process.env.BASE_URL}/api/products/${id}`);
  const result: ProductModel = await response.json();
  return result;
};

type GetProductsResponse = ProductModel[];
interface getProductsProps<T> {
  options?: UseQueryOptions<GetProductsResponse, any, T, any>;
}

export function useGetProducts<T = GetProductsResponse>(props?: getProductsProps<T>) {
  return useQuery(['products'], getProducts, props?.options);
}

type GetProductResponse = ProductModel;
interface getProductProps<T> {
  id: string;
  options?: UseQueryOptions<GetProductResponse, any, T, any>;
}

export function useGetProduct<T = GetProductResponse>({ id, ...props }: getProductProps<T>) {
  return useQuery(['product', { id }], () => getProduct(id), props?.options);
}
