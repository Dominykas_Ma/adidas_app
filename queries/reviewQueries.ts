import unfetch from 'isomorphic-unfetch';
import { ReviewModel } from './types';
import { useMutation, useQuery, useQueryClient, UseQueryOptions } from 'react-query';

export const getReviews = async (productId: string) => {
  const response = await unfetch(`${process.env.BASE_URL}/api/reviews/${productId}`);
  const result: ReviewModel[] = await response.json();
  return result;
};

type GetReviewsResponse = ReviewModel[];
interface getReviewsProps<T> {
  id: string;
  options?: UseQueryOptions<GetReviewsResponse, any, T, any>;
}

export function useGetProductReviews<T = GetReviewsResponse>({ id, ...props }: getReviewsProps<T>) {
  return useQuery(['reviews', { id }], () => getReviews(id), props?.options);
}

type ReviewParams = {
  productId: string;
  rating: number;
  text: string;
};

interface setReviewProps {
  options?: UseQueryOptions<GetReviewsResponse, any, ReviewParams, any>;
}

export function useSetProductReview(props?: setReviewProps) {
  const queryClient = useQueryClient();
  return useMutation(
    async (params: ReviewParams) => {
      const response = await unfetch(`${process.env.BASE_URL}/api/reviews/${params.productId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      });
      return response.json();
    },
    {
      //Optimistic update
      onMutate: async (data) => {
        await queryClient.cancelQueries(['reviews']);
        queryClient.setQueryData<ReviewParams>(['reviews'], (prevData) => {
          if (!prevData) {
            return data;
          }
          return { ...prevData, data };
        });
      },
      onSettled: () => {
        queryClient.invalidateQueries(['reviews']);
      },
      ...props?.options,
    }
  );
}
