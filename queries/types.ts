export interface ProductModel {
  id: string;
  name: string;
  currency: string;
  price: number;
  description: string;
  imgUrl: string;
}

export interface ReviewModel {
  productId: string;
  locale: string;
  rating: number;
  text: string;
}
