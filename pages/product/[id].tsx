import React from 'react';
import { getProduct, getProducts, useGetProduct } from '../../queries/productQueries';
import Navbar from '../../components/Navbar';
import ProductDetailsCard from '../../components/ProductDetailsCard';
import Reviews from '../../components/Reviews';
import { Box } from '@chakra-ui/react';
import Footer from '../../components/Footer';
import ContentWrapper from '../../components/ContentWrapper';
import AddReview from '../../components/AddReview';
import { QueryClient, dehydrate } from 'react-query';
import { getReviews, useGetProductReviews } from '../../queries/reviewQueries';
import { useRouter } from 'next/router';
import { GetStaticProps } from 'next';

const Product = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data: product } = useGetProduct({ id: id as string });
  const { data: reviews } = useGetProductReviews({ id: id as string });
  return (
    <>
      <Navbar type={'BACK'} />
      <ContentWrapper>
        {product && (
          <>
            <ProductDetailsCard
              id={product.id}
              name={product.name}
              currency={product.currency}
              price={product.price}
              description={product.description}
              imgUrl={product.imgUrl}
            />
            <Box pt={5}>
              <AddReview productId={product.id} />
            </Box>
          </>
        )}
        {reviews && <Reviews reviews={reviews} />}
      </ContentWrapper>
      <Footer />
    </>
  );
};

export async function getStaticPaths() {
  const products = await getProducts();
  // generate the paths
  const paths = products.map((product) => ({
    params: { id: product.id },
  }));
  return {
    paths,
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(['product'], () => getProduct(params!.id as string));
  await queryClient.prefetchQuery(['reviews'], () => getReviews(params!.id as string));

  return { props: { dehydratedState: dehydrate(queryClient) } };
};

export default Product;
