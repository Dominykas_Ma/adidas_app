import nc from 'next-connect';
import cors from 'cors';
import { NextApiRequest, NextApiResponse } from 'next';

const handler = nc()
  // use connect based middleware
  .use(cors())
  .get(async (req: NextApiRequest, res: NextApiResponse) => {
    const response = await fetch('http://localhost:3001/product');
    const data = await response.json();
    res.json(data);
  });

export default handler;
