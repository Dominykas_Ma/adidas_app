import nc from 'next-connect';
import cors from 'cors';
import { NextApiRequest, NextApiResponse } from 'next';
import unfetch from 'isomorphic-unfetch';

const handler = nc()
  // use connect based middleware
  .use(cors())
  .get(async (req: NextApiRequest, res: NextApiResponse) => {
    const { productId } = req.query;
    const response = await unfetch(`http://localhost:3001/product/${productId}`);
    const data = await response.json();
    res.json(data);
  });

export default handler;
