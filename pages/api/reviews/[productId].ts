import nc from 'next-connect';
import cors from 'cors';
import { NextApiRequest, NextApiResponse } from 'next';
import unfetch from 'isomorphic-unfetch';

const handler = nc()
  // use connect based middleware
  .use(cors())
  .get(async (req: NextApiRequest, res: NextApiResponse) => {
    const { productId } = req.query;
    const response = await unfetch(`http://localhost:3002/reviews/${productId}`);
    const data = await response.json();
    res.json(data);
  })
  .post(async (req: NextApiRequest, res: NextApiResponse) => {
    const { productId } = req.query;
    const response = await unfetch(`http://localhost:3002/reviews/${productId}`, {
      method: 'POST',
      body: JSON.stringify(req.body),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json();
    res.json(data);
  });

export default handler;
