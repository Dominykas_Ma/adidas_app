import Head from 'next/head';
import Navbar from '../components/Navbar';
import ProductCard from '../components/ProductCard';
import { Alert, AlertIcon, Box, chakra, SimpleGrid } from '@chakra-ui/react';
import React, { useState } from 'react';
import { dehydrate, QueryClient } from 'react-query';
import { getProducts, useGetProducts } from '../queries/productQueries';
import Footer from '../components/Footer';
import ContentWrapper from '../components/ContentWrapper';

const Home = () => {
  const [keyword, setKeyword] = useState('');
  const { data, isError } = useGetProducts({
    options: {
      select: (productData) =>
        productData.filter((product) => product.name.toLowerCase().startsWith(keyword.toLowerCase())),
    },
  });

  return (
    <>
      <Head>
        <title>Adidas Product App</title>
        <meta name="description" content="Adidas Product App" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar type={'SEARCH'} onChange={(value) => setKeyword(value)} />
      <ContentWrapper color={'gray.700'}>
        <>
          <Box py={4}>
            <chakra.h1
              color={'white'}
              fontWeight="bold"
              fontSize="3xl"
              textTransform="uppercase"
              textAlign={{ base: 'center', md: 'left' }}
            >
              Products
            </chakra.h1>
          </Box>
          {isError && (
            <Alert status="error">
              <AlertIcon borderRadius={5} />
              There was an error processing your request, please try again
            </Alert>
          )}
          <SimpleGrid minChildWidth={'20rem'} spacingX="30px" spacingY="30px">
            {data && data.length > 0 ? (
              data.map((product, index) => (
                <ProductCard
                  id={product.id}
                  key={index}
                  name={product.name}
                  currency={product.currency}
                  price={product.price}
                  description={product.description}
                  imgUrl={product.imgUrl}
                />
              ))
            ) : (
              <chakra.h1
                color={'white'}
                fontWeight="bold"
                fontSize="3xl"
                textTransform="uppercase"
                textAlign={{ base: 'center', md: 'left' }}
                pt={50}
              >
                No results found...
              </chakra.h1>
            )}
          </SimpleGrid>
        </>
      </ContentWrapper>
      <Footer />
    </>
  );
};

export async function getStaticProps() {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(['products'], getProducts);
  return { props: { dehydratedState: dehydrate(queryClient) } };
}

export default Home;
